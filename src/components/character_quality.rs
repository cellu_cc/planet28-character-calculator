use yew::prelude::*;
use crate::state::*;

#[derive(PartialEq, Properties, Clone)]
pub struct CharacterQualityProps {
    pub character_quality_index: u32
  , pub quality: CharacterQuality
  , pub onremove: Callback<u32>
}

#[function_component(CharacterQualityComponent)]
pub fn character_trait(props: &CharacterQualityProps) -> Html {
    let onclick_remove= {
        let onremove = props.onremove.clone();
        let character_trait_index = props.character_quality_index.clone();
        move |_| onremove.emit(character_trait_index)
    };
    html!{
      <tr>
          <td style="vertical-align: middle;">{props.quality.name.clone()}</td>
          <td style="vertical-align: middle;">{props.quality.description.clone()}</td>
          <td style="vertical-align: middle;">{props.quality.get_point_value().clone()}</td>
          <td style="vertical-align: middle;"><button class="button is-ghost" onclick={onclick_remove}>{"Remove"}</button></td>
      </tr>
    }
}
