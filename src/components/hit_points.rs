use yew::prelude::*;
use crate::state::*;

#[derive(PartialEq, Properties, Clone)]
pub struct HitPointsProps {
    pub value: u32
  , pub onchange: Callback<u32>
}

#[function_component(HitPoints)]
pub fn attribute(props: &HitPointsProps) -> Html {
  let onclick_increment = {
      let onchange = props.onchange.clone();
      let value = props.value.clone();
      move |_| onchange.emit(value+2)
};
  let onclick_decrement = {
      let onchange = props.onchange.clone();
      let value = props.value.clone();
      move |_| onchange.emit(value-2)
  };
  html!{
      <div class="field has-addons">
          <p class="control"> <button class="button is-primary is-inverted has-text-weight-bold" disabled={props.value == 2} onclick={onclick_decrement}> {"-"}</button> </p>
          <p class="control"> <input class="input has-text-centered" type="text" value={props.value.to_string()}/> </p>
          <p class="control"> <button class="button is-primary is-inverted has-text-weight-bold" onclick={onclick_increment}> {"+"}</button> </p>
      </div>
    }
}