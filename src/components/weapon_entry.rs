use yew::prelude::*;
use crate::state::*;

#[derive(PartialEq, Properties, Clone)]
pub struct WeaponEntryProps {
    pub weapon_entry_index: u32
  , pub weapon: Weapon 
  , pub onremove: Callback<u32>
}

#[function_component(WeaponEntryComponent)]
pub fn weapon_entry_component(props: &WeaponEntryProps) -> Html {
    let onclick_remove= {
        let onremove = props.onremove.clone();
        let weapon_entry_index = props.weapon_entry_index.clone();
        move |_| onremove.emit(weapon_entry_index)
    };
    html!{
      <tr>
          <td style="vertical-align: middle;">{props.weapon.name.clone()}</td>
          <td style="vertical-align: middle;"><div class="tags">
              { for props.weapon.weapon_type.iter().map( |weapon_type|
                  html!{<span class="tag">{weapon_type.to_string().clone()}</span>}
              )}
          </div></td>
          <td style="vertical-align: middle;">{props.weapon.weapon_size().clone()}</td>
          <td style="vertical-align: middle;">{props.weapon.damage_string().clone()}</td>
          <td style="vertical-align: middle;"><div class="tags">
              { for props.weapon.special_rules.iter().map( |weapon_special_rule|
                  html!{
                      <abbr title={weapon_special_rule.effect.to_string().clone()}>
                          <span class="tag">
                              {weapon_special_rule.name.to_string().clone()}
                              {" ("}{weapon_special_rule.cost.to_string().clone()}{")"}
                          </span>
                      </abbr>
                  }
              )}
          </div></td>
          <td style="vertical-align: middle;">{props.weapon.range_cm.to_string().clone()}</td>
          <td style="vertical-align: middle;">{props.weapon.point_value().to_string().clone()}</td>
          <td style="vertical-align: middle;"><button class="button is-ghost" onclick={onclick_remove}>{"Remove"}</button></td>
      </tr>
    }
}