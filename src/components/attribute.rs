use yew::prelude::*;
use crate::state::*;

#[derive(PartialEq, Properties, Clone)]
pub struct AttributeProps {
    pub attribute_id: u32
  , pub value: u32
  , pub onchange: Callback<(u32,u32)>
}

#[function_component(Attribute)]
pub fn attribute(props: &AttributeProps) -> Html {
  let onclick_increment = {
      let onchange = props.onchange.clone();
      let attribute_id = props.attribute_id.clone();
      let value = props.value.clone();
      move |_| onchange.emit((attribute_id, value+1))
  };
  let onclick_decrement = {
      let onchange = props.onchange.clone();
      let attribute_id = props.attribute_id.clone();
      let value = props.value.clone();
      move |_| onchange.emit((attribute_id, value-1))
  };
  html!{
      <div class="block m-1">
          <p class="has-text-centered has-text-weight-semibold mb-1"><abbr title={Character::attribute_description_from_id(props.attribute_id)}>{Character::attribute_name_from_id(props.attribute_id)}</abbr></p>
          <div class="field has-addons">
              <p class="control"> <button class="button is-primary is-inverted has-text-weight-bold" disabled={props.value == 1} onclick={onclick_decrement}> {"-"}</button> </p>
              <p class="control"> <input class="input has-text-centered" type="text" value={props.value.to_string()}/> </p>
              <p class="control"> <button class="button is-primary is-inverted has-text-weight-bold" disabled={props.value == 9} onclick={onclick_increment}> {"+"}</button> </p>
          </div>
      </div>
    }
}