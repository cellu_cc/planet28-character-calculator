use web_sys::{HtmlInputElement, HtmlSelectElement, HtmlAnchorElement};
use yew::prelude::*;
use gloo::storage::{LocalStorage, Storage};
use gloo::net::http::Request;
use gloo::file::Blob;
use gloo::console::log;
use serde_json::Result;
use wasm_bindgen::JsCast;



use crate::state::{Action, State, CharacterQuality, Weapon};
use crate::components::hit_points::HitPoints as HitPoints;
use crate::components::attribute::Attribute as Attribute;
use crate::components::character_quality::CharacterQualityComponent;
use crate::components::weapon_entry::WeaponEntryComponent as WeaponEntryComponent;
mod state;
mod components;
// Define the possible messages which can be sent to the component


const KEY: &str = "yew.planet28charactercalculator.self";

#[function_component(App)]
fn app() -> Html {
    let state = use_reducer(|| {
        LocalStorage::get(KEY).unwrap_or_else(|_| State::new())
    });

    let file_state = use_state(|| {
        state::FileState { upload_file: None }
    });

    // Effect
    use_effect_with_deps(
        move |state| {
            LocalStorage::set(KEY, &(*state.clone())).expect("failed to set");
            || ()
        },
        state.clone(),
    );

    use_effect_with_deps(move |state| {
        let state = state.clone();
        wasm_bindgen_futures::spawn_local(async move {
            let fetched_character_traits: Vec<CharacterQuality> = Request::get("/character_traits.json")
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            state.dispatch(Action::UpdateAvailableCharacterTraits(fetched_character_traits))
        });
        || ()
    }, state.clone());

    use_effect_with_deps(move |state| {
        let state = state.clone();
        wasm_bindgen_futures::spawn_local(async move {
            let fetched_character_abilities: Vec<CharacterQuality> = Request::get("/character_abilities.json")
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            state.dispatch(Action::UpdateAvailableCharacterAbilities(fetched_character_abilities))
        });
        || ()
    }, state.clone());

    use_effect_with_deps(move |state| {
        let state = state.clone();
        wasm_bindgen_futures::spawn_local(async move {
            let fetched_character_weapons: Vec<Weapon> = Request::get("/character_weapons.json")
                .send()
                .await
                .unwrap()
                .json()
                .await
                .unwrap();
            state.dispatch(Action::UpdateAvailableCharacterWeapons(fetched_character_weapons))
        });
        || ()
    }, state.clone());


    let onchanged_name = {
        let state = state.clone();
        Callback::from(move |e: Event| { 
            let input = e.target_dyn_into::<HtmlInputElement>();
            if let Some(new_name) = input { state.dispatch(Action::SetName(new_name.value()))}
        })
    };
    let onchanged_attribute = {
        let state = state.clone();
        Callback::from(move |(id, value): (u32, u32)| {
            state.dispatch(Action::SetAttributeValue((id, value)))
        })
    };

    let onchanged_hp = {
        let state = state.clone();
        Callback::from(move |value: u32| {
            state.dispatch(Action::SetHealthPoints(value))
        })
    };

    let onclick_remove_trait = {
        let state = state.clone();
        Callback::from(move |index: u32| {
            state.dispatch(Action::RemoveTrait(index))
        })
    };

    let onchange_add_trait = {
        let state = state.clone();
        Callback::from(move |e: Event|{
            let select_into = e.target_dyn_into::<HtmlSelectElement>();
            let Some(select) = select_into else {return};
            let Ok(u32_index) = usize::from_str_radix(&select.value(), 10) else {return};
            let character_trait = state.available_character_traits[u32_index].clone();
            state.dispatch(Action::AddTrait(character_trait))
        })
    };
    
    let onclick_remove_ability = {
        let state = state.clone();
        Callback::from(move |index: u32| {
            state.dispatch(Action::RemoveAbility(index))
        })
    };

    let onchange_add_ability = {
        let state = state.clone();
        Callback::from(move |e: Event|{
            let select_into = e.target_dyn_into::<HtmlSelectElement>();
            let Some(select) = select_into else {return};
            let Ok(u32_index) = usize::from_str_radix(&select.value(), 10) else {return};
            let character_ability = state.available_character_abilities[u32_index].clone();
            state.dispatch(Action::AddAbility(character_ability))
        })
    };

    let onclick_remove_weapon = {
        let state = state.clone();
        Callback::from(move |index: u32| {
            state.dispatch(Action::RemoveWeapon(index))
        })
    };

    let onchange_add_weapon = {
        let state = state.clone();
        Callback::from(move |e: Event|{
            let select_into = e.target_dyn_into::<HtmlSelectElement>();
            let Some(select) = select_into else {return};
            let Ok(u32_index) = usize::from_str_radix(&select.value(), 10) else {return};
            let weapon = state.available_character_weapons[u32_index].clone();
            state.dispatch(Action::AddWeapon(weapon))
        })
    };

    let onclick_new = {
        let state = state.clone();
        Callback::from(move |_| {
            if state.request_new {                
                state.dispatch(Action::ConfirmNew)
            } else {                
                state.dispatch(Action::RequestNew)
            }
        })
    };

    let onclick_upload = {
        //let state = state.clone();
        Callback::from(move |_| {
            let window = web_sys::window().expect("global window does not exists");    
		    let document = window.document().expect("expecting a document on window");
		    let file_upload_element = document.get_element_by_id("uploadFile")
		        .unwrap()
		        .dyn_into::<web_sys::HtmlInputElement>()
		        .unwrap();
            file_upload_element.click();
            ()
        })
    };

    let onchange_file_upload = {
        let file_state = file_state.clone();
        let state = state.clone();
        Callback::from(move |e: Event|{
            log!("file uploaded");
            let file_input_into = e.target_dyn_into::<HtmlInputElement>();
            let Some(file_input) = file_input_into else { return };
            let Some(file_list) = file_input.files() else { return };
            let Some(file) = file_list.item(0) else { return };
            let gloo_file: gloo::file::File = file.into();
            let state = state.clone();
            //let file_text = file.text();
            //let text_result = wasm_bindgen_futures::JsFuture::from(file_text);
            file_state.set(state::FileState {
                upload_file: Some(
                    gloo::file::callbacks::read_as_text(&gloo_file, move |res| {
                        let Ok(content) = res else {return};
                        let json_state_result:Result<state::Character> = serde_json::from_str(&content[..]);
                        let Ok(json_state) = json_state_result else {return};
                        state.dispatch(Action::SetCharacter(json_state))
                    }
                 ))
                }
              );
            //log!(file.name());
            
            ()
        })
    };

    html!{
        <section class="section">
            <div class="container is-max-desktop">
                <nav class="navbar" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <h1 class="title"> {"Planet28 Character Calculator"} </h1>
                    </div>
                    <div class="navbar-menu">
                        <div class="navbar-end">
                            <button class="button is-ghost" onclick={onclick_new}>{
                                if state.request_new {
                                    "Confirm New?"
                                } else {
                                    "New"
                                }
                            }</button> 
                            <a href={ 
                                let json_state = serde_json::to_string(&state.character).unwrap();
                                let state_string = wasm_bindgen::JsValue::from_str(&json_state[..]);
                                let array = js_sys::Array::new();
                                array.push(&state_string);
                                let blob = web_sys::Blob::new_with_str_sequence_and_options(
                                    &array
                                  , web_sys::BlobPropertyBag::new().type_("application/json"),
                                  ).unwrap();
                                let download_url = web_sys::Url::create_object_url_with_blob(&blob).unwrap();
                                download_url
                              }
                                class="button is-ghost"
                                download="character.json">{"Download"}
                            </a>
                            <input type="file" id="uploadFile" style="display:none;" onchange={onchange_file_upload}/>
                            <button class="button is-ghost" onclick={onclick_upload}>{"Upload"}</button>
                        </div>
                    </div>
                </nav>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Name"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input 
                                    class="input" 
                                    type="text" 
                                    placeholder="Name"
                                    onchange={onchanged_name}
                                    value={state.character.name.clone()}
                                />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Points"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input 
                                    class="input" 
                                    type="text" 
                                    placeholder="Points" 
                                    readonly=true 
                                    value={state.character.calculate_point_value().to_string()}
                                />
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal level">
                    <div class="field-label is-normal">
                        <label class="label">{"Attributes"}</label>
                    </div>
                    <div class="field-body">
                        { for state.character.attributes.iter().enumerate().map(|(id, attribute)| 
                            html! {
                                <Attribute 
                                    attribute_id={id as u32} 
                                    value={attribute} 
                                    onchange={onchanged_attribute.clone()}
                                />
                            })
                        }   
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Speed"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" type="text" value={state.character.get_speed()} readonly=true/>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Hit-Points"}</label>
                    </div>
                    <div class="field-body">
                        <HitPoints 
                            value={state.character.hit_points} 
                            onchange={onchanged_hp.clone()}
                        />
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Traits"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                            <table class="table is-fullwidth mb-1">
                                <thead><tr><th>{"Name"}</th><th>{"Description"}</th><th>{"Points"}</th><th></th></tr></thead>
                                { for state.character.traits.iter().enumerate().map(|(index, character_trait)| 
                                    html! {
                                        <CharacterQualityComponent
                                            character_quality_index={index as u32} 
                                            quality={character_trait.clone()}
                                            onremove={onclick_remove_trait.clone()}
                                        />
                                    })
                                }  
                            </table>
                            <div class="select" onchange={onchange_add_trait.clone()}><select>
                                { for state.available_character_traits.iter().enumerate().map(|(index, character_trait)|
                                    html!{
                                        <option value={index.to_string()} title={character_trait.description.clone()}>{format!("{1} ({0:+2})", character_trait.point_value, character_trait.name.clone())}</option>
                                    }
                                )}
                                <option>{"Add a Trait"}</option>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Abilities"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                            <table class="table is-fullwidth mb-1">
                                <thead><tr><th>{"Name"}</th><th>{"Effect"}</th><th>{"Points"}</th><th></th></tr></thead>
                                { for state.character.abilities.iter().enumerate().map(|(index, character_ability)| 
                                    html! {
                                        <CharacterQualityComponent
                                            character_quality_index={index as u32} 
                                            quality={character_ability.clone()} 
                                            onremove={onclick_remove_ability.clone()}
                                        />
                                    })
                                }  
                            </table>
                            <div class="select" onchange={onchange_add_ability.clone()}><select>
                                { for state.available_character_abilities.iter().enumerate().map(|(index, character_ability)|
                                    html!{
                                        <option 
                                            value={index.to_string()} 
                                            title={character_ability.description.clone()}
                                        >
                                            { format!( 
                                                  "{1} ({0:+2})"
                                                , character_ability.point_value
                                                , character_ability.name.clone()
                                                )
                                            }
                                        </option>
                                    }
                                )}
                                <option>{"Add an Ability"}</option>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Weapons"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                        <table class="table is-fullwidth mb-1">
                            <thead><tr><th>{"Name"}</th><th>{"Type"}</th><th>{"Size"}</th><th>{"Damage"}</th><th>{"Special Rules"}</th><th>{"Range"}</th><th>{"Cost"}</th><th></th></tr></thead>
                            { for state.character.weapons.iter().enumerate().map(|(index, weapon)| 
                                html! {
                                    <WeaponEntryComponent
                                        weapon_entry_index={index as u32}
                                        weapon={weapon.clone()}
                                        onremove={onclick_remove_weapon.clone()}
                                    /> 
                                }
                            )}
                        </table>
                        <div class="select" onchange={onchange_add_weapon.clone()}><select>
                                { for state.available_character_weapons.iter().enumerate().map(|(index, weapon)|
                                    html!{
                                        <option value={index.to_string()}>{weapon.name.clone()}</option>
                                    }
                                )}
                                <option>{"Add a Weapon"}</option>
                            </select></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    }
}

/*
pub struct App {
    state: State
  , 
}
impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let state = State{
            name: "".to_string()
          , points: 0
          , hit_points: 0 
          , attributes: [0;5]
          };
        Self { state: state }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SetName(new_name) => { 
                self.state.name = new_name; 
                true
            }
            Msg::IncrementAttribute => {
                self.state.points += 1;
                true // Return true to cause the displayed change to update
            }
            Msg::DecrementAttribute => {
                self.state.points -= 1;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <section class="section">
                <div class="container">
                    <h1 class="title"> {"Planet28 Character Calculator"} </h1>
                </div>
                {self.view_name_field(ctx.link())}
            </section>
        }
    }
}

impl App{
    fn view_name_field(&self, link: &Scope<Self>) -> Html {
        let on_change = link.batch_callback(|e: Event| {
            let input:Option<HtmlInputElement> = e.target_dyn_into::<HtmlInputElement>();
            if let Some(input) = input {
                return Some(Msg::SetName(input.value()))
            } else {
                return None
            }
        });
        html!{
            <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Name"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input 
                                    class="input" 
                                    type="text" 
                                    placeholder="Name"
                                    onchange={on_change}
                                    value={self.state.name.clone()}
                                />
                            </p>
                        </div>
                    </div>
                </div>
            }
    }
}
 */
fn main() {
    yew::Renderer::<App>::new().render();
}
