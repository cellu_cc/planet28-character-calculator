use serde_derive::{Deserialize, Serialize};
use yew::prelude::*;
use std::rc::Rc;
use gloo::console::log;
use gloo::file::callbacks::FileReader;

use crate::components::weapon_entry::_WeaponEntryProps;

//use strum_macros::{Display, EnumIter};

pub struct FileState {
    pub upload_file: Option<FileReader>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct State {
    pub character: Character
  , pub available_character_traits: Vec<CharacterQuality>
  , pub available_character_abilities: Vec<CharacterQuality>
  , pub available_character_weapons: Vec<Weapon>
  , pub request_new: bool
  }

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Character {
    pub name: String
  , pub attributes: [u32; 5]
  , pub hit_points: u32
  , pub traits: Vec<CharacterQuality>
  , pub abilities: Vec<CharacterQuality>
  , pub weapons: Vec<Weapon>
  }

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct CharacterQuality {
    pub name: String
  , pub description: String
  , pub point_value: i32
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct Weapon {
    pub name: String
  , pub weapon_type: Vec<WeaponType>
  , pub size: WeaponSize
  , pub num_dice: u32
  , pub dice_size: u32
  , pub modifier: i32
  , pub special_rules: Vec<WeaponSpecialRule>
  , pub range_cm: u32
  , pub cost: Option<u32>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct WeaponSpecialRule {
    pub name: String
  , pub effect: String
  , pub cost: i32
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub enum WeaponSize{
    OneHanded
  , TwoHanded
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub enum WeaponType{
    Ballistic
  , Complex
  , Energy
  , Melee
  , Psychic
  , Ranged
}

impl Weapon{
    pub fn weapon_size(&self) -> String {
        match self.size {
            WeaponSize::OneHanded => return "One-Handed".to_string()
          , WeaponSize::TwoHanded => return "Two-Handed".to_string()
        }
    }

    pub fn point_value(&self) -> i32 {
        if let Some(point_value) = self.cost { return point_value as i32 }
        let base_points = ((self.num_dice*self.dice_size) as i32)+self.modifier + self.range_cm as i32;
        let special_rules_points = self.special_rules.iter().map(|rule| rule.cost).sum::<i32>();
        let weapon_type_points:i32 = match self.size {
            WeaponSize::OneHanded => 10
          , WeaponSize::TwoHanded => 0
        };
        return base_points + special_rules_points + weapon_type_points
    }

    pub fn damage_string(&self) -> String {
        let dice_string = format!("{}D{}", self.num_dice, self.dice_size);
        let modifier_string = if self.modifier != 0 { format!("{:+2}", self.modifier) } else { "".to_string() };
        return format!("{}{}", dice_string, modifier_string);
    }
}

impl WeaponType {
    pub fn to_string(&self) -> String {
        match self {
            WeaponType::Ballistic => "Ballistic".to_string()
          , WeaponType::Complex => "Complex".to_string()
          , WeaponType::Energy => "Energy".to_string()
          , WeaponType::Melee => "Melee".to_string()
          , WeaponType::Psychic => "Psychic".to_string()
          , WeaponType::Ranged => "Ranged".to_string()
        }
    }
}

impl CharacterQuality {
    pub fn get_point_value(&self) -> String {
        if self.point_value > 0 { return format!("+{}",self.point_value) }
        return self.point_value.to_string()
    }
}

pub enum Action{
    SetName(String)
  , SetAttributeValue((u32,u32))
  , SetHealthPoints(u32)
  , AddTrait(CharacterQuality)
  , RemoveTrait(u32)
  , AddAbility(CharacterQuality)
  , RemoveAbility(u32)
  , AddWeapon(Weapon)
  , RemoveWeapon(u32)
  , UpdateAvailableCharacterTraits(Vec<CharacterQuality>)
  , UpdateAvailableCharacterAbilities(Vec<CharacterQuality>)
  , UpdateAvailableCharacterWeapons(Vec<Weapon>)
  , RequestNew
  , ConfirmNew
  , SetCharacter(Character)
  }

impl State{
    pub fn new() -> Self {
        State{ 
            character: Character::new()
          , available_character_traits: Vec::new()
          , available_character_abilities: Vec::new()
          , available_character_weapons: Vec::new()
          , request_new: false
        }
    }
}

impl Character {
  pub fn new() -> Self {
    Character{
      name: "".to_string()
    , hit_points: 20
    , attributes: [1;5]
    , traits: Vec::new()
    , abilities: Vec::new()
    , weapons: Vec::new()
    }
  }
  pub fn calculate_point_value(&self) -> u32{
      let base_point_value:i32 = 10;
      let attributes_point_value:u32 = self.attributes.iter().map(|attribute| (attribute-1)*10).sum::<u32>();
      let hit_points_point_value = (self.hit_points as i32 -20)*5;
      let character_traits_point_value = self.traits.iter().map(|character_trait| character_trait.point_value).sum::<i32>();
      let weapons_point_value = self.weapons.iter().map(|other_weapon| other_weapon.point_value()).sum::<i32>();
      return ( 
          base_point_value
        + hit_points_point_value
        + character_traits_point_value
        + weapons_point_value 
        + attributes_point_value as i32
        ).max(10) as u32
  }

  pub fn attribute_name_from_id(id:u32) -> String {
      match id {
          0 => "Agility (A)".to_string()
        , 1 => "Fighting (F)".to_string()
        , 2 => "Shooting (S)".to_string()
        , 3 => "Awareness (AW)".to_string()
        , 4 => "Psyche (P)".to_string()
        , _ => "".to_string()
      }
  }
  pub fn attribute_description_from_id(id:u32) -> String {
    match id {
        0 => "How well a character moves and navigates their environment".to_string()
      , 1 => "How well a character fights in hand to hand combat".to_string()
      , 2 => "How well a character fights using ranged weapons and firearms".to_string()
      , 3 => "How well a character responds to their surroundings".to_string()
      , 4 => "How intelligent a character is, and how well they use or resist psychic powers".to_string()
      , _ => "".to_string()
      }
  }
  fn valid_attribute_id(id:u32) -> bool {
      return id < 5 
  }

  fn valid_attribute_value(value:u32) -> bool {
      return value >= 1 && value < 10
  }

  pub fn get_speed(&self) -> String {
      let mut speed = 10;
      let mut modifier_string = "".to_string();
      for character_trait in self.traits.iter(){
          if character_trait.name.to_lowercase() == "big" {
              speed = speed - 3;
          }
          if character_trait.name.to_lowercase() == "slow" {
              modifier_string = " - 1D6".to_string();
          }
          if character_trait.name.to_lowercase() == "fast" {
              modifier_string = " + 1D6".to_string();
          }
      }
      return speed.to_string() + &modifier_string
  }
}

impl Reducible for State {
    type Action = Action;
    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        match action{
            Action::RequestNew => {
                log!("Requesting new character");
                State { 
                    request_new: true
                  , ..(*self).clone()
                  }.into()
            }
          , Action::ConfirmNew => {
                log!("Confirming new character");
                State::new().into()
            }
          , Action::SetCharacter(character) => { 
                State {
                    character: character
                  , request_new: false
                  , ..(*self).clone()
                }.into()
            }
          , Action::SetName(name) => {
                log!("Setting name");
                State {
                    character: Character {
                        name: name
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::SetAttributeValue((id, new_value)) => {
                log!("Setting attribute value");
                let mut new_attributes = self.character.attributes.clone();
                if Character::valid_attribute_id(id) && Character::valid_attribute_value(new_value) { 
                    new_attributes[id as usize] = new_value 
                }
                State {
                    character: Character {
                        attributes: new_attributes
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into() 
            }
          , Action::SetHealthPoints(new_hp_value) => {                
                log!("Setting HP value");
                State {
                    character: Character{
                        hit_points: new_hp_value
                      , ..self.character.clone()
                      }
                  , ..(*self).clone()
                  }.into()
            }
          , Action::AddTrait(new_trait) => {            
                log!("Adding Trait");
                let mut new_traits = self.character.traits.clone();
                new_traits.push(new_trait);
                State {
                    character: Character{
                        traits: new_traits
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::RemoveTrait(trait_index) => {
                let mut new_traits = self.character.traits.clone();
                new_traits.remove(trait_index as usize);
                State {
                    character: Character{
                        traits: new_traits
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::AddAbility(new_ability) => {            
                log!("Adding ability");
                let mut new_abilities = self.character.abilities.clone();
                new_abilities.push(new_ability);
                State {
                    character: Character{
                        abilities: new_abilities
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::RemoveAbility(ability_index) => {            
                log!("Removing Ability");
                let mut new_abilities = self.character.abilities.clone();
                new_abilities.remove(ability_index as usize);
                State {
                    character: Character{
                        abilities: new_abilities
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::AddWeapon(new_weapon) => {
                let mut new_weapons = self.character.weapons.clone();
                new_weapons.push(new_weapon);
                State {
                    character: Character{
                        weapons: new_weapons
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::RemoveWeapon(weapon_index) => {
                let mut new_weapons = self.character.weapons.clone();
                new_weapons.remove(weapon_index as usize);
                State {
                    character: Character{
                        weapons: new_weapons
                      , ..self.character.clone()
                      }
                  , request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::UpdateAvailableCharacterTraits(new_character_traits) => {
                State{
                    available_character_traits: new_character_traits
                  //, request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::UpdateAvailableCharacterAbilities(new_character_abilities) => {
                State{
                    available_character_abilities: new_character_abilities
                  //, request_new: false
                  , ..(*self).clone()
                  }.into()
            }
          , Action::UpdateAvailableCharacterWeapons(new_character_weapons) => {
                State{
                    available_character_weapons: new_character_weapons
                  //, request_new: false
                  , ..(*self).clone()
                  }.into()
            }
        }
    }
}