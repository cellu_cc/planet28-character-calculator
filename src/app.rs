use yew::prelude::*;

#[function_component(App)]
pub fn app() -> Html {
    html! {
            <div class="container is-max-desktop">
                <h1 class="title"> {"Planet28 Character Calculator"} </h1>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Name"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" type="text" placeholder="Name"/>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Points"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" type="text" placeholder="Points" readonly=true/>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal level">
                    <div class="field-label is-normal">
                        <label class="label">{"Attributes"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block m-1">
                            <p class="has-text-centered has-text-weight-semibold mb-1"><abbr title="Test">{"Agility (A)"}</abbr></p>
                            <div class="field has-addons is-danger">
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                                <p class="control"> <input class="input has-text-centered" type="text" placeholder="1"/> </p>
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                            </div>
                        </div>
                        <div class="block m-1">
                            <p class="has-text-centered has-text-weight-semibold mb-1">{"Fighting (F)"}</p>
                            <div class="field has-addons">
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                                <p class="control"> <input class="input has-text-centered" type="text" placeholder="1"/> </p>
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                            </div>
                        </div>
                        <div class="block m-1">
                            <p class="has-text-centered has-text-weight-semibold mb-1">{"Shooting (S)"}</p>
                            <div class="field has-addons">
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                                <p class="control"> <input class="input has-text-centered" type="text" placeholder="1"/> </p>
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                            </div>
                        </div>
                        <div class="block m-1">
                            <p class="has-text-centered has-text-weight-semibold mb-1">{"Awareness (AW)"}</p>
                            <div class="field has-addons">
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                                <p class="control"> <input class="input has-text-centered" type="text" placeholder="1"/> </p>
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                            </div>
                        </div>
                        <div class="block m-1">
                            <p class="has-text-centered has-text-weight-semibold mb-1">{"Psyche (P)"}</p>
                            <div class="field has-addons">
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                                <p class="control"> <input class="input has-text-centered" type="text" placeholder="1"/> </p>
                                <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Speed"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input class="input" type="text" placeholder="10 cm or 4\"" readonly=true/>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Hit-Points"}</label>
                    </div>
                    <div class="field-body">
                        <div class="field has-addons">
                            <p class="control"> <a class="button is-primary has-text-weight-bold"> {"-"}</a> </p>
                            <p class="control"> <input class="input has-text-centered" type="text" placeholder="20"/> </p>
                            <p class="control"> <a class="button is-primary has-text-weight-bold"> {"+"}</a> </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Traits"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                            <table class="table is-fullwidth mb-1">
                                <thead><tr><th>{"Name"}</th><th>{"Description"}</th><th>{"Points"}</th><th></th></tr></thead>
                                <tr>
                                    <td>{"Sniper"}</td>
                                    <td>{"This character may reroll any failed shooting rolls once"}</td>
                                    <td>{"+17"}</td>
                                    <td>{"Remove"}</td>
                                </tr>
                            </table>
                            <div class="select"><select>
                                <option>{"Rageful"}</option>
                                <option>{"Slow"}</option>
                                <option>{"Add a Trait"}</option>
                            </select></div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Abilities"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                        <table class="table is-fullwidth mb-1">
                            <thead><tr><th>{"Name"}</th><th>{"Description"}</th><th>{"Points"}</th><th></th></tr></thead>
                            <tr>
                                <td>{"Aimed Shot"}</td>
                                <td>{"This character may use an action to aim their weapon at an enemies' weak spot. Their weapon does an additional +1D8 damage in their next attack this turn."}</td>
                                <td>{"+10"}</td>
                                <td>{"Remove"}</td>
                            </tr>
                        </table>
                        <div class="select"><select>
                            <option>{"Drain"}</option>
                            <option>{"Heal"}</option>
                            <option>{"Add an Ability"}</option>
                        </select></div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Weapons"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                        <table class="table is-fullwidth mb-1">
                            <thead><tr><th>{"Name"}</th><th>{"Type"}</th><th>{"Size"}</th><th>{"Damage"}</th><th>{"Special Rules"}</th><th>{"Range"}</th><th>{"Cost"}</th><th></th></tr></thead>
                            <tr>
                                <td>{"Sniper Rifle"}</td>
                                <td><div class="tags"><span class="tag">{"Ranged"}</span><span class="tag">{"Ballistic"}</span></div></td>
                                <td>{"Two-Handed"}</td>
                                <td>{"2D10"}</td>
                                <td>{""}</td>
                                <td>{"60 cm or 24\""}</td>
                                <td>{"80"}</td>
                                <td>{"Remove"}</td>
                            </tr>
                        </table>
                        <div class="select"><select>
                            <option>{"Shotgun"}</option>
                            <option>{"Plasma Rifle"}</option>
                            <option>{"Add a Weapon"}</option>
                        </select></div>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">{"Armor"}</label>
                    </div>
                    <div class="field-body">
                        <div class="block is-fullwidth is-flex-grow-1">
                        <table class="table is-fullwidth mb-1">
                            <thead><tr><th>{"Armor"}</th><th>{"Defense Rating"}</th><th>{"Details"}</th><th>{"Cost"}</th><th></th></tr></thead>
                            <tr>
                                <td>{"Ballistic Plate"}</td>
                                <td>{"1D8+4"}</td>
                                <td>{""}</td>
                                <td>{"10"}</td>
                                <td>{"Remove"}</td>
                            </tr>
                        </table>
                        <div class="select"><select>
                            <option>{"Living Armor"}</option>
                            <option>{"Power Armor"}</option>
                            <option>{"Add Armor"}</option>
                        </select></div>
                        </div>
                    </div>
                </div>
            </div>
    }
}
